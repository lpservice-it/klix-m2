FROM php:7.4-fpm

USER root

COPY --from=composer:2.1.12 /usr/bin/composer /usr/local/bin/composer
RUN apt-get update --fix-missing
RUN apt-get install -y \
    build-essential \
    libssl-dev \
    zlib1g-dev \
    libpng-dev \
    libjpeg-dev \
    libfreetype6-dev \
    libsodium-dev \
    libicu-dev \
    libcurl4-openssl-dev \
    libonig-dev \
    libxml2-dev \
    libzip-dev \
    libxslt-dev

RUN docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/

RUN docker-php-ext-install \
    sodium \
    gd \
    intl \
    curl \
    json \
    mbstring \
    xml \
    zip \
    soap \
    bcmath \
    pdo_mysql \
    xsl \
    sockets

RUN cd /usr/local/etc/php/conf.d/ && \
  echo 'memory_limit = 4G' >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini

ENV PHP_MEMORY_LIMIT=4G
