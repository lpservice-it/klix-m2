# Magento 2.4.3 klix test project

## Install guide

### Clone repository and cd into project directory

```
git clone git@bitbucket.org:lpservice-it/klix-m2.git klix-citadele
cd klix-citadele
```

#### Before you start

1. Create an account or login at https://marketplace.magento.com
2. Copy "./src/auth.json.sample" as "./src/auth.json"
3. Generate new or copy existing access keys to "auth.json" from https://marketplace.magento.com/customer/accessKeys/
4. Create a vhost "127.0.0.1  magento2.host"

#### 1. Run docker compose

For Linux:
```
docker-compose up -d --build
```

For Mac:
```
docker-sync start
docker-compose up -f docker-compose.mac.yml -d --build
```

NOTE: docker-sync available in gem
`gem install docker-sync`


#### 2. Bash into docker fpm and cd into www root

```
docker-compose exec fpm bash
cd /var/www/html
```

#### 3. Run composer install

```
composer install
```

#### 4. Run magento install

```
php bin/magento setup:install \
--base-url=http://magento2.host/ \
--db-host=db \
--db-name=magento2 \
--db-user=magento2 \
--db-password=magento2 \
--admin-firstname=admin \
--admin-lastname=admin \
--admin-email=admin@admin.com \
--admin-user=admin \
--admin-password=admin123 \
--language=en_US \
--currency=EUR \
--timezone=Europe/Riga \
--use-rewrites=1 \
--elasticsearch-host=elasticsearch \
--backend-frontname=admin
```

#### 5. Enable developer mode and deploy sample data

```
php bin/magento deploy:mode:set developer
php bin/magento sampledata:deploy
php bin/magento module:enable Magento_CustomerSampleData Magento_MsrpSampleData Magento_CatalogSampleData Magento_DownloadableSampleData Magento_OfflineShippingSampleData Magento_BundleSampleData Magento_ConfigurableSampleData Magento_ThemeSampleData Magento_ProductLinksSampleData Magento_ReviewSampleData Magento_CatalogRuleSampleData Magento_SwatchesSampleData Magento_GroupedProductSampleData Magento_TaxSampleData Magento_CmsSampleData Magento_SalesRuleSampleData Magento_SalesSampleData Magento_WidgetSampleData Magento_WishlistSampleData
```

#### 6. Disable 2FA module

```
php bin/magento module:disable Magento_TwoFactorAuth
```

#### 7. Run upgrade, reindex and clear cache

```
php bin/magento setup:upgrade
php bin/magento index:reindex
php bin/magento cache:flush
```

#### 8. Fix permissions

```
chown -R www-data:www-data .
```

Magento 2 should now be accessible from http://magento2.host/

Admin url: http://magento2.host/admin
- username: admin
- password: admin123

## Klix plugin activation

#### 1. Go to Magento admin

```
Stores -> Settings -> Configuration
```

#### 2. Move to Payment Method section:

```
Sales -> Payment Methods
```

#### 4. Find "Spell E-commerce Gateway" and configure it.

#### 5. Click save and clear cache (System -> Cache Management)

## Use with Ngrok

Assuming that you have Ngrok running and know your URL:

#### 1. Bash into docker fpm and cd into www root

```
docker-compose exec fpm bash
cd /var/www/html
```

#### 2. Set Magento base URL

```
php bin/magento setup:store-config:set --base-url="<NGROK URL>"
php bin/magento setup:store-config:set --base-url-secure="<NGROK URL>"
```

#### 3. Flush Magento cache

```
php bin/magento cache:flush
```

Magento should now be accessible from your Ngrok URL.
